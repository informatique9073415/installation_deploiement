# Utilisation de l'image de base Alpine
FROM alpine:latest

# Variables d'environnement pour le proxy et les versions
ARG HTTP_PROXY
ARG HTTPS_PROXY
ARG NO_PROXY
ARG ANSIBLE_VERSION
ARG TERRAFORM_VERSION

ENV HTTP_PROXY=$HTTP_PROXY
ENV HTTPS_PROXY=$HTTPS_PROXY
ENV NO_PROXY=$NO_PROXY

# Mise à jour des dépôts
RUN apk update

# Installation des prérequis
RUN apk add --no-cache python3 py3-pip wget unzip

# Installation de Ansible
RUN pip3 install ansible==${ANSIBLE_VERSION}

# Installation de Terraform
RUN wget https://releases.hashicorp.com/terraform/${TERRAFORM_VERSION}/terraform_${TERRAFORM_VERSION}_linux_amd64.zip
RUN unzip terraform_${TERRAFORM_VERSION}_linux_amd64.zip -d /usr/bin/
RUN rm terraform_${TERRAFORM_VERSION}_linux_amd64.zip

# Nettoyage
RUN apk del wget unzip && rm -rf /var/cache/apk/*
