#!/bin/bash
# Configure le proxy pour le système

source ../config/proxy.cfg

export http_proxy=$HTTP_PROXY
export https_proxy=$HTTPS_PROXY
export no_proxy=$NO_PROXY
