#!/bin/bash

# Obtenir le chemin du script
SCRIPT_PATH="$(dirname $(readlink -f $0))"
# Fonction pour gérer l'authentification
auth() {
    local username=""
    local password=""
    dir_auth="auth"
    file_auth="auth.ini"

    while (( "$#" )); do
        case "$1" in
            -u|--username)
                if [ -z "$2" ] || [[ "$2" == -* ]]; then
                    echo "Erreur: Argument attendu pour $1"
                    exit 1
                fi
                username="$2"
                shift 2
                ;;
            -p|--password)
                if [ -z "$2" ] || [[ "$2" == -* ]]; then
                    echo "Erreur: Argument attendu pour $1"
                    exit 1
                fi
                password="$2"
                shift 2
                ;;
            *)
                echo "Option inconnue: $1"
                exit 1
                ;;
        esac
    done

    # Si le nom d'utilisateur n'est pas fourni, demandez-le
    if [ -z "$username" ]; then
        echo -n "Veuillez saisir votre nom d'utilisateur Openstack: "
        read username
    fi

    # Si le mot de passe n'est pas fourni, demandez-le
    if [ -z "$password" ]; then
        echo -n "Veuillez saisir votre mot de passe Openstack: "
        read -s password  # -s option hides the input
        echo  # print a newline after the password input
    fi

    echo "Authentification de l'utilisateur $username..."
    
    # Create the auth directory if it does not exist

    mkdir -p $SCRIPT_PATH/$dir_auth

    # Write the username and password to the auth.ini file
    cat > $SCRIPT_PATH/$dir_auth/$file_auth << EOF
[Auth Openstack]
username = $username
password = $password
EOF

    echo "Enregistrement des informations d'authentification effectué dans $SCRIPT_PATH/$dir_auth/$file_auth"
}

# Fonction pour enregistrer une nouvelle configuration
register() {
    local name interactive
    while [ $# -gt 0 ]; do
        case "$1" in
            --name|-n)
                shift
                name="$1"
                ;;
            --interactive|-it)
                interactive=1
                ;;
            *)
                echo "Erreur: Option inconnue: $1"
                exit 1
                ;;
        esac
        shift
    done

    if [ -z "$name" ]; then
        # Retrieve the list of projects
        local projects="$(openstack project list)"
        # Initialize an associative array to store project names and IDs
        declare -A project_array
        # Counter for project choices
        local counter=1
        # Parse the list of projects
        while read -r line; do
            # Extract the project ID and name using regex
            if [[ "$line" =~ ^\|(.+)\|(.+)\|$ ]]; then
                local id="${BASH_REMATCH[1]// }"  # Remove spaces
                local name="${BASH_REMATCH[2]// }"  # Remove spaces
                # Add the project to the array
                project_array[$counter]="$id:$name"
                counter=$((counter+1))
            fi
        done <<< "$projects"
        # Display the list of projects for the user to choose from
        echo "Choix | ID | Nom"
        for choice in "${!project_array[@]}"; do
            local id_name=(${project_array[$choice]//:/ })
            echo "$choice | ${id_name[0]} | ${id_name[1]}"
        done
        # Ask the user to choose a project
        echo -n "Quel projet voulait vous choisir ? "
        read choice
        if [ -z "${project_array[$choice]}" ]; then
            echo "Erreur: Choix invalide"
            exit 1
        fi
        id_name=(${project_array[$choice]//:/ })
        name="${id_name[1]}"
    fi

    echo "Enregistrement du projet $name..."
    # TODO: Implement the project registration logic here
}

<<com
register() {
    local name=""
    local interactive=0  # Utilisé comme booléen (0 pour faux, 1 pour vrai)

    while (( "$#" )); do
        case "$1" in
            -n|--name)
                if [ -z "$2" ] || [[ "$2" == -* ]]; then
                    echo "Erreur: Argument attendu pour $1"
                    exit 1
                fi
                name="$2"
                shift 2
                ;;
            -it|--interactive)
                interactive=1
                shift
                ;;
            *)
                echo "Option inconnue: $1"
                exit 1
                ;;
        esac
    done

    if [ -z "$name" ]; then
        echo "Voici la liste des projets à ajouter"
        
    fi

    echo "Enregistrement du projet $name..."
    if (( interactive )); then
        echo "Mode interactif activé..."
        # Votre logique d'enregistrement interactif ici...
    else
        echo "Mode interactif désactivé..."
        # Votre logique d'enregistrement standard ici...
    fi
}
com
# Fonction pour supprimer une configuration
remove() {
    local name=""
    local all=false

    while (( "$#" )); do
        case "$1" in
            -n|--name)
                if [ -z "$2" ] || [[ "$2" == -* ]]; then
                    echo "Erreur: Argument attendu pour $1"
                    exit 1
                fi
                name="$2"
                shift 2
                ;;
            -a|--all)
                all=true
                shift
                ;;
            *)
                echo "Option inconnue: $1"
                exit 1
                ;;
        esac
    done

    if [ -z "$name" ]; then
        echo "Voici la liste des projets à supprimer"
        
    fi

    if [ "$all" = true ]; then
        echo "Suppression de toutes les configurations pour le projet $name"
        # Votre logique de suppression ici...
    else
        echo "Voici la liste des configurations à supprimer"
        echo "Suppression de la configuration pour le projet $name..."
        # Votre logique de suppression ici...
    fi
}

# Fonction pour choisir une configuration
choice() {
    local name=""
    local config=""

    while (( "$#" )); do
        case "$1" in
            -n|--name)
                if [ -z "$2" ] || [[ "$2" == -* ]]; then
                    echo "Erreur: Argument attendu pour $1"
                    exit 1
                fi
                name="$2"
                shift 2
                ;;
            -c|--config)
                if [ -z "$2" ] || [[ "$2" == -* ]]; then
                    echo "Erreur: Argument attendu pour $1"
                    exit 1
                fi
                config="$2"
                shift 2
                ;;
            *)
                echo "Option inconnue: $1"
                exit 1
                ;;
        esac
    done

    if [ -z "$name" ]; then
        echo "Voici la liste des projets à choisir"
    
    fi

    if [ -z "$config" ]; then
        echo "Voici la liste des configurations à choisir"

    fi

    echo "Choix du projet $name avec la configuration $config..."
    # Votre logique de choix ici...
}

# Fonction pour gérer l'affichage des informations
show() {
    local basic=true
    local all=false
    local auth=false

    while (( "$#" )); do
        case "$1" in
            -a|--all)
                all=true
                auth=true
                shift
                ;;
            --auth)
                auth=true
                basic=false
                shift
                ;;
            *)
                echo "Option inconnue: $1"
                exit 1
                ;;
        esac
    done


    
    # Votre logique ici...
    if [ "$basic" = true ]; then
        echo "Affichage de la configuration en cours"
    fi

    if [ "$auth" = true ]; then
        echo "Affichage des informations d'authentification..."
        # Votre logique ici...
    fi
}

# Fonction pour appliquer une configuration
apply() {
    local confirm=""

    while (( "$#" )); do
        case "$1" in
            -y|--yes)
                confirm="yes"
                shift
                ;;
            *)
                echo "Option inconnue: $1"
                exit 1
                ;;
        esac
    done

    # Si la confirmation n'est pas fournie, demandez-la
    if [ -z "$confirm" ]; then
        echo -n "Veuillez confirmer l'application de la configuration [yes/no]: "
        read confirm
    fi

    # Convertir l'entrée en minuscules
    confirm=$(echo "$confirm" | tr '[:upper:]' '[:lower:]')

    # Vérifiez si la réponse est 'y', 'yes', 'n' ou 'no'
    case "$confirm" in
        y|yes)
            echo "Application de la configuration..."
            # Votre logique d'application de configuration ici...
            ;;
        n|no)
            echo "Application de la configuration annulée."
            ;;
        *)
            echo "Erreur : réponse non reconnue. Veuillez répondre par 'yes' ou 'no'."
            exit 1
            ;;
    esac
}

# Traitement des arguments de ligne de commande
while (( "$#" )); do
    case $1 in
        auth) shift; auth "$@"; break ;;
        register) shift; register "$@"; break ;;
        remove) shift; remove "$@"; break ;;
        choice) shift; choice "$@"; break ;;
        show) shift; show "$@"; break ;;
        apply) shift; apply "$@"; break ;;
        *) echo "Commande inconnue: $1"; exit 1 ;;
    esac
done