#!/bin/bash
# Script pour installer Ansible et Terraform sur différents systèmes d'exploitation

source ../config/versions.cfg
source configure_proxy.sh


# Vérifier si Ansible est installé et si sa version correspond à ANSIBLE_VERSION
check_ansible() {
  if command -v ansible >/dev/null; then
    INSTALLED_ANSIBLE_VERSION=$(ansible --version | grep "ansible [core" | cut -d ' ' -f 4 | tr -d '[]')
    if [ "$INSTALLED_ANSIBLE_VERSION" == "$ANSIBLE_VERSION" ]; then
      return 0
    else
      echo "Une autre version de Ansible est installée : $INSTALLED_ANSIBLE_VERSION."
      return 1
    fi
  else
    return 1
  fi
}

# Vérifier si Terraform est installé et si sa version correspond à TERRAFORM_VERSION
check_terraform() {
  if command -v terraform >/dev/null; then
    INSTALLED_TERRAFORM_VERSION=$(terraform version | head -n 1 | cut -d 'v' -f 2)
    if [ "$INSTALLED_TERRAFORM_VERSION" == "$TERRAFORM_VERSION" ]; then
      return 0
    else
      echo "Une autre version de Terraform est installée : $INSTALLED_TERRAFORM_VERSION."
      return 1
    fi
  else
    return 1
  fi
}

# Détecter le système d'exploitation
OS="$(uname -s)"

echo "Système d'exploitation détecté : $OS"


# Option Docker
if [ "$1" == "docker" ]; then
  echo "Installation dans un conteneur Docker..."
  docker build --build-arg HTTP_PROXY=$HTTP_PROXY \
                --build-arg HTTPS_PROXY=$HTTPS_PROXY \
                --build-arg NO_PROXY=$NO_PROXY \
                --build-arg ANSIBLE_VERSION=$ANSIBLE_VERSION \
                --build-arg TERRAFORM_VERSION=$TERRAFORM_VERSION \
                -t ansible-terraform:latest ../
  docker run -it --name ansible-terraform ansible-terraform:latest
else
  # Installer Ansible et Terraform sur Linux
  if [ "$OS" == "Linux" ]; then
    if ! check_ansible; then
      echo "Installation de Ansible..."
      sudo apt-add-repository --yes --update ppa:ansible/ansible
      sudo apt install -y ansible=$ANSIBLE_VERSION
    fi
    if ! check_terraform; then
      echo "Installation de Terraform..."
      curl -fsSL https://apt.releases.hashicorp.com/gpg | sudo apt-key add -
      sudo apt-add-repository "deb [arch=amd64] https://apt.releases.hashicorp.com $(lsb_release -cs) main"
      sudo apt-get update && sudo apt-get install terraform=$TERRAFORM_VERSION
    fi
    ansible --version
    terraform -v
  # Installer Ansible et Terraform sur MacOS
  elif [ "$OS" == "Darwin" ]; then
    if ! check_ansible; then
      echo "Installation de Ansible..."
      brew install ansible
    fi
    if ! check_terraform; then
      echo "Installation de Terraform..."
      brew install terraform
    fi
    ansible --version
    terraform -v
  else
    echo "Votre système d'exploitation n'est pas supporté."
  fi

fi
